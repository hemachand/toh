var fs = require('fs');

var moment = require('moment');

//var conf = {'from':'Tak Över Huvudet <info@takoverhuvudet.se>','host':'toh01.takoverhuvudet.se','port':25,'username':'info@takoverhuvudet.se','password':'takoverhuvudet'};
var conf = {
				'from':'Tak Över Huvudet <info@takoverhuvudet.se>',
				'host':'smtp.takoverhuvudet.se',
				'port':587,
				'username':'send_smtp@takoverhuvudet.se',
				'password':'TakOverHuvudet@2015'
			};

var mailOptions = {

	confirmation : function(userAccount, confirmURL)
	{
		userAccount = userAccount.toJSON();
		userProfile = userAccount.profile.toJSON();
		var confirmMsg = fs.readFileSync(__dirname + '/../views/content/templates/registration_email.html','utf8');
		console.log("Confirmation Email Content "+confirmMsg);
		confirmMsg = confirmMsg.replace('$$user$$', userProfile.accountType == 'private' ? userProfile.name : userProfile.corporation);
		confirmMsg = confirmMsg.replace('$$confirm_link$$', confirmURL);

		options = {
			from: conf.from,
			to: userAccount.username,
			subject: 'Välkommen till TakÖverHuvudet bekräfta e-post',
			html: confirmMsg
		}
		return options;
	}
	, passwordRecovery : function(userAccount, confirmURL){
		userAccount = userAccount.toJSON();
		var password_recovery_msg = fs.readFileSync(__dirname + '/../views/content/templates/password_recovery.html','utf8');
		console.log("Password Recovery Email Content "+password_recovery_msg);
		password_recovery_msg = password_recovery_msg.replace('$$user$$', userAccount.profile.name);
		password_recovery_msg = password_recovery_msg.replace('$$confirm_link$$', confirmURL);
		options = {
				from: conf.from,
				to: userAccount.username,
				subject: 'Återskapa lösenord post',
				html: password_recovery_msg
			}
			return options;
	},sendMessage : function(userAccount){
//		userAccount = userAccount.toJSON();
		console.log("sendMessage :::::::::: >>>>>>>>>>>");
		console.log(userAccount);
		userProfile = userAccount.profile;
		var new_message_email = fs.readFileSync(__dirname + '/../views/content/templates/newmessage_email.html','utf8');
		new_message_email = new_message_email.replace('$$user$$', userProfile.accountType == 'private' ? userProfile.name : userProfile.corporation);
		console.log("New Message Email content after parsing "+new_message_email);
		options = {
				from: conf.from,
				to: userAccount.username,
				subject: 'Nytt meddelande från TakÖverHuvudet',
				html: new_message_email
			}
			return options;
	},
	send_receipt : function(userAccount, adObj, paymentData){
		userAccount = userAccount.toJSON();
		var send_receipt_html ='';
		var sender_type = '';
		if(userAccount.profile.corporation){
			send_receipt_html = fs.readFileSync(__dirname + '/../views/content/templates/receipt_corporation.html','utf8');
			send_receipt_html = send_receipt_html.replace('$$corporationName$$', userAccount.profile.corporation);
			send_receipt_html = send_receipt_html.replace('$$corporationName$$', userAccount.profile.corporation);
			send_receipt_html = send_receipt_html.replace('$$responsiblePerson$$', userAccount.profile.responsiblePerson);
			send_receipt_html = send_receipt_html.replace('$$address$$', userAccount.profile.street);
			send_receipt_html = send_receipt_html.replace('$$postalCode$$', userAccount.profile.postalcode);
			send_receipt_html = send_receipt_html.replace('$$city$$', userAccount.profile.city);
			send_receipt_html = send_receipt_html.replace('$$phone$$', userAccount.profile.phone);
			send_receipt_html = send_receipt_html.replace('$$corporationID$$', userAccount.profile.corpID);
			send_receipt_html = send_receipt_html.replace('$$receiptID$$', adObj.get('payment_receipt'));
			send_receipt_html = send_receipt_html.replace('$$adID$$', adObj.get('id'));
			send_receipt_html = send_receipt_html.replace('$$date$$', moment().locale('sv').format("dddd, Do MMMM YYYY, h:mm:ss a"));

		}else{
			send_receipt_html = fs.readFileSync(__dirname + '/../views/content/templates/receipt_private.html','utf8');
			send_receipt_html = send_receipt_html.replace('$$firstName$$', userAccount.profile.name);
			send_receipt_html = send_receipt_html.replace('$$firstName$$', userAccount.profile.name);
			send_receipt_html = send_receipt_html.replace('$$familyName$$', userAccount.profile.familyname);
			send_receipt_html = send_receipt_html.replace('$$address$$', userAccount.profile.street);
			send_receipt_html = send_receipt_html.replace('$$postalCode$$', userAccount.profile.postalcode);
			send_receipt_html = send_receipt_html.replace('$$city$$', userAccount.profile.city);
			send_receipt_html = send_receipt_html.replace('$$phone$$', userAccount.profile.phone);
			send_receipt_html = send_receipt_html.replace('$$corporationID$$', userAccount.profile.corpID);
			send_receipt_html = send_receipt_html.replace('$$receiptID$$', adObj.get('payment_receipt'));
			send_receipt_html = send_receipt_html.replace('$$adID$$', adObj.get('id'));
			send_receipt_html = send_receipt_html.replace('$$date$$', moment().locale('sv').format("dddd, Do MMMM YYYY, h:mm:ss a"));
		}
//		console.log("Sending Receipt HTML Content "+send_receipt_html);
		options = {
				from: conf.from,
				to: userAccount.username,
				cc: 'kvitto@takoverhuvudet.se,meijuvartiainen@gmail.com',
				subject: 'Kvitto från Tak över huvudet',
				html: send_receipt_html
			}
		return options;
	}
};

var replaceAll = function (find, replace, str) {
  var find = find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  return str.replace(new RegExp(find, 'g'), replace);
}

module.exports.conf = conf;
module.exports.mailOptions = mailOptions;
