//custom library
//model
var account = require('../model/account')
, profileTable = require('../model/profile')
, ads = require('../model/ads')
, matchedAds = require('../model/matchedAds').matchedAds
, messageCounter = require('../model/message-counter').messageCounter;

var constants = require('../model/constants');

var fs = require('fs');

var crypto = require('crypto');
var https = require('https');
var nodemailer = require("nodemailer"),
    smtpTransport = require('nodemailer-smtp-transport');

var logger =  require('../log');

//vendor library
var moment = require('moment');
var mailconf = require('./mailconf.js');
var mailService = require('../services/mail-service.js')
var notAuthorisedJSON = {code: '1', message: 'auth_failure'};
var failueJSON = {code: '2', message: 'failure'};

var confirmMailAddress = function(req, res, next) {
	if(req.params.authKey){
		new account.user({accountKey: req.params.authKey}).updateStatus(account.status.active).then(function(){
			res.redirect('/home');
		}, function(){
			sendFailureJSON(req, res, next);
		});	
	}
	else if(req.session.authKey){
		new account.user({accountKey: req.session.authKey}).fetch({withRelated: ['profile']}).then(function(userAccount){
			if(userAccount){
				var confirmURL = req.protocol + '://' + req.get('host') + '/account/confirm/'+userAccount.get('accountKey');
				//Sending Mail for Registered User
				var mailOptions=mailconf.mailOptions.confirmation(userAccount, confirmURL);
				console.log(mailOptions);
				mailService.sendMail(mailOptions);
				//Sending Mail for Registered User
			}
			sendSuccessJSON(req, res, next, {message: 'Mail sent successfully'});
		}, function(){
			sendFailureJSON(req, res, next);
		});
	}
}

var passwordRecovery = function(req, res, next) {
	userData = req.body;
	console.log(userData)
	new account.user({username: userData.userEmail}).fetch({withRelated: ['profile']}).then(function(userAccount){
		if(userAccount){
			var confirmURL = req.protocol + '://' + req.get('host') + '/account/password/'+userAccount.get('accountKey');
			//Sending Mail for Registered User
			var mailOptions=mailconf.mailOptions.passwordRecovery(userAccount, confirmURL);
			console.log(mailOptions);
			mailService.sendMail(mailOptions);
			//Sending Mail for Registered User
			sendSuccessJSON(req, res, next, {message: 'success'});
		}
		else {
			sendFailureJSON(req, res, next, {message: 'invalid_user'});
		}
	}, function(){
		sendFailureJSON(req, res, next, {message: 'invalid_user'});
	});
}

var changePassword = function(req, res, next) {
	var newPassword = req.body.newPassword ? req.body.newPassword : req.params.newPassword;
	var userAccountKey = req.session.authKey ? req.session.authKey : req.params.authKey;
	if(!newPassword && req.params.authKey){
		req.session.accountKey = req.params.authKey;
		res.render('content/setNewPassword.html');
	}
	else {
		new account.user({accountKey: userAccountKey}).changePassword(newPassword).then(function(){
			sendSuccessJSON(req, res, next);
		}, function(){
			sendFailureJSON(req, res, next);
		});	
	}
}

var deleteAccount = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).updateStatus(account.status.deleted).then(function(){
		//TODO: all open matched ads of the user to be removed and mapped ads status to be changed.
		req.logout();
		sendSuccessJSON(req, res, next);
	}, function(){
		sendFailureJSON(req, res, next);
	})
}

var getProfile = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch({columns: ['id', 'username', 'signupdate','status'], withRelated:['profile','messageCount']})
	.then(function(userAccountObj){
		logger.log('info', 'Profile Object',userAccountObj);
		sendSuccessJSON(req, res, next, {userAccount: userAccountObj});
	});
}

var putProfile = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		userAccountObj = userAccountObj.toJSON();
		if(userAccountObj){
			new profileTable.userProfile({account_id: userAccountObj.id}).updateProfile(req.body, req.files, userAccountObj).then(function(userprofileObj){
				sendSuccessJSON(req, res, null, {userAccount: userAccountObj, userprofile: userprofileObj});
				//sendMail(userAccountObj);
			});
		}
	});
}

var getAds = function(req, res, next){
	new account.user({accountKey: req.session.authKey}).fetch({withRelated:['messageCount']}).then(function(userAccountObj){
		ads.ads.where({account_id : userAccountObj.get('id')}).fetchAll({withRelated:['adData','adImages']}).then(function(adObjs){
			sendSuccessJSON(req, res, next, {ads: adObjs, userAccount : userAccountObj});
		}, function(error){
			sendFailureJSON(req, res, next, error)
		});
	});
}

var getLettingAds = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		ads.ads.where({account_id : userAccountObj.get('id'), adType:'letting' }).fetchAll({withRelated:['adData']}).then(function(adObjs){
			sendSuccessJSON(req, res, next, {ads: adObjs, userAccount : userAccountObj});
		}, function(error){
			sendFailureJSON(req, res, next, error)
		});
	});
}

var getRentingAds = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		ads.ads.where({account_id : userAccountObj.get('id'), adType:'renting' }).fetchAll({withRelated:['adData']}).then(function(adObjs){
			sendSuccessJSON(req, res, next, {ads: adObjs, userAccount : userAccountObj});
		}, function(error){
			sendFailureJSON(req, res, next, error)
		});
	});
}

var getAdData = function(req, res, next){
	logger.log('info', 'Getting Ad Data');
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		ads.ads.where({account_id : userAccountObj.get('id'), id: req.params.adId }).fetch({withRelated:['adData', 'adImages']}).then(function(adObj){
			if(adObj){
				sendSuccessJSON(req, res, next, {ad: adObj, userAccount : userAccountObj});
			}
			else{
				sendFailureJSON(req, res, next)
			}
		}, function(error){
			sendFailureJSON(req, res, next, error)
		});
	});
}

var postAd = function(req, res, next) {
	var adData = req.body;
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		adData['account_id'] = userAccountObj.get('id');
		adData['lastupdatedtime'] = moment().valueOf();
		adData['creationtime'] = moment().valueOf();
		new ads.ads(adData).add().then(function(adObj){
			var files = req.files;
			logger.log('info', 'Files in Posting Ad',files);
			ads.addOrUpdateImagesToAd(files, userAccountObj.get('accountKey'), adObj.id);
			sendSuccessJSON(req, res, next, {ad: adObj, userAccount : userAccountObj});	
		}, function(error){
			console.log('error :: '+JSON.stringify(error));
			sendFailureJSON(req, res, next, error);
		});	
	});
}

var putAd = function(req, res, next) {
	if(!req.params.adId){
		sendFailureJSON(req, res, next, error);
	}
	var adData = req.body;
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		adData['account_id'] = userAccountObj.get('id');
		adData['lastupdatedtime'] = moment().valueOf();
		adData['id'] = req.params.adId;
		new ads.ads({id: req.params.adId, account_id : userAccountObj.get('id')}).update(adData).then(function(adObj){
			var files = req.files;
			logger.log('info', 'Files in Updating Ad',files);
			ads.addOrUpdateImagesToAd(files, userAccountObj.get('accountKey'), adObj.id);
			console.log("adObj after then ::" +JSON.stringify(adObj));
			sendSuccessJSON(req, res, next, {ad: adObj, userAccount : userAccountObj});	
		}, function(error){
			console.log('error :: '+JSON.stringify(error));
			sendFailureJSON(req, res, next, error);
		});
	});
}

var deleteAd = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		var searchObj = {account_id : userAccountObj.get('id'), id: req.params.adId};
		new ads.ads(searchObj).fetch().then(function(adObj){
			if(adObj){
				var adStatusCodes = constants.status.ads;
				console.log(adObj.get('adStatus'));
				if(adObj.get('adStatus') == adStatusCodes.matched){
					var statusCodes = constants.status.matchedAds;
					var query = matchedAds.query({where: {renting_ad_id : adObj.get('id')}, orWhere:{letting_ad_id : adObj.get('id')}}).fetch()
					.then(function(foundEntry){
						console.log(JSON.stringify(foundEntry.get('status')));
						if(foundEntry){
							console.log('DeleteAd ::::: There exists a matching for the ad :: '+req.params.adId);
							foundEntryJSON = foundEntry.toJSON();
							adObj.where(searchObj).destroy().then(function(){ sendSuccessJSON(req, res, next); }, function(err){ sendFailureJSON(req, res, next, err); });
							ads.updateAdStatus(foundEntry.toJSON(), adStatusCodes.open);
						}
						else {
							console.log('DeleteAd ::::: There is no matching for the ad :: '+req.params.adId);
							adObj.where(searchObj).destroy().then(function(){ sendSuccessJSON(req, res, next); }, function(err){ sendFailureJSON(req, res, next, err); });	
						}
					})
				}
				else {
					adObj.where(searchObj).destroy().then(function(){ sendSuccessJSON(req, res, next); }, function(err){ sendFailureJSON(req, res, next, err); });
				}
			}
			else {
				sendFailureJSON(req, res, next, {message: 'Invalid Ad'});
			}
		})
	});
}

var postPaymentForAd = function(req, res, next) {
	var data = req.body;
	console.log(data);
	new account.user({accountKey: req.session.authKey}).fetch({withRelated: ['profile']}).then(function(userAccountObj){
		var searchObj = {account_id : userAccountObj.get('id'), id: req.params.adId};
		new ads.ads(searchObj).fetch().then(function(adObj){
			if(adObj){
				adObj.where(searchObj).save({payment_receipt: data.transact}, {method:'update', patch: true});
				adObj.updateAdStatus(ads.adStatusConstants.open);
				
				new ads.ads(searchObj).fetch().then(function(adObj){
					console.log('adObj for mail options :: '+JSON.stringify(adObj));
					if(adObj){
						var mailOptions=mailconf.mailOptions.send_receipt(userAccountObj, adObj);
//						console.log(mailOptions);
						mailService.sendMail(mailOptions);
					}
				})
				res.redirect(data.callbackurl);
			}
			else {
				sendFailureJSON(req, res, next, {message: 'Invalid Ad'});
			}
		});
	});
}

var postPayment = function(req, res, next) {
	var data = req.body;
	var dataToPostJSON = {amount: data.amount, merchantId: data.merchant, transactionId: data.transact};
	var dataToPost = 'amount='+data.amount+'&merchantId='+data.merchant+'&transactionId='+data.transact;
	var hexDecode = new Buffer('415948337a37242d374b5e32574256717d2d4745367a2436652456253125626c7b5f333a69755d65515a6459216a3b59554038465d6a465d3a59235a2a695623', 'hex').toString('utf8');
	console.log('hexDecode :: '+hexDecode);
	var hmac = crypto.createHmac('sha256', hexDecode).update(dataToPost).digest('hex');
	console.log(dataToPost);
	console.log(hmac);
	dataToPostJSON['MAC'] = hmac;
	var options = { hostname: 'api.dibspayment.com', path: '/merchant/v1/JSON/Transaction/CaptureTransaction?data='+JSON.stringify(dataToPostJSON), method: 'POST' };
	var hreq = https.request(options, function(hres) {
		console.log("statusCode: ", hres.statusCode);
		console.log("headers: ", hres.headers);
		var payload = '';
		hres.on('data', function(d) {
			payload += d;
			payload = JSON.parse(payload);
			console.log(payload);
			new account.user({accountKey: req.session.authKey}).fetch({withRelated: ['profile']}).then(function(userAccountObj){
				var searchObj = {account_id : userAccountObj.get('id'), id: data.orderid};
				new ads.ads(searchObj).fetch().then(function(adObj){
					if(adObj){
						adObj.where(searchObj).save({payment_receipt: data.transact}, {method:'update', patch: true});
						if(payload.status == 'ACCEPT'){
							adObj.updateAdStatus(ads.adStatusConstants.open);
							new ads.ads(searchObj).fetch().then(function(adObj){
								console.log('adObj for mail options :: '+JSON.stringify(adObj));
								if(adObj){
									var mailOptions=mailconf.mailOptions.send_receipt(userAccountObj, adObj);
									console.log(mailOptions);
									mailService.sendMail(mailOptions);
								}
							})
						}
						res.redirect(data.callbackurl);
					}
					else {
						sendFailureJSON(req, res, next, {message: 'Invalid Ad'});
					}
				});
			});
		});
	});
	console.log(dataToPostJSON.toString);
	hreq.end();

	hreq.on('error', function(e) {
		console.error(e);
		res.redirect(req.body.declineurl);
	});
}

var getMessages = function(req, res, next) {
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		matchedAds.query({where: {renting_account_id : userAccountObj.get('id')}})
		.where('status', 'in', ['awaiting', 'renterDenied', 'letterDenied', 'autoDenied', 'renterAccepted', 'approved'])
		.fetchAll({withRelated:['lettingAdData', 'letterProfile', {'letter': function(qb) { qb.select('account.id', 'account.username');}}, 'letter.profile']}).then(function(rentingAdObjs){
			matchedAds.query({where: {letting_account_id : userAccountObj.get('id')}})
			.where('status', 'in', ['renterAccepted', 'letterDenied', 'autoDenied', 'approved'])
			.fetchAll({withRelated:['rentingAdData', 'rentingAdData.adImages', 'renterProfile'
			                        , {'renter': function(qb) { qb.select('account.id', 'account.username');}}, 'renter.profile']})
			                        .then(function(lettingAdObjs){
			                        	new messageCounter({account_id: userAccountObj.get('id')}).reinitCount(userAccountObj.get('id'));
			                        	sendSuccessJSON(req, res, next, {'rentingAdObjs':rentingAdObjs, 'lettingAdObjs': lettingAdObjs, userAccount: userAccountObj});
			                        });
		})
	});
}

var updateMessageStatus = function(req, res, next) {
	var action = req.params.action;
	var actionOnWhichAdType = req.params.adType;
	var adId = req.params.adId;
	var matchedAdId = req.params.matchedAdId;
	var statusCodes = constants.status.matchedAds;
	new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
		var criteria = {};
		var status;
		var statusCriteria;
		if(actionOnWhichAdType == 'letting'){
			criteria = {letting_account_id : userAccountObj.get('id'), letting_ad_id: adId};
			status = action == 'accept' ? statusCodes.letterAccepted : (action == 'deny' ? statusCodes.letterDenied : statusCodes.letterDeleted);
			if(matchedAdId){
				criteria['renting_ad_id'] = matchedAdId;
			}
		}
		else if(actionOnWhichAdType == 'renting'){
			criteria = {renting_account_id : userAccountObj.get('id'), renting_ad_id: adId};
			status = action == 'accept' ? statusCodes.renterAccepted : (action == 'deny' ? statusCodes.renterDenied : statusCodes.renterDeleted);
			if(matchedAdId){
				criteria['letting_ad_id'] = matchedAdId;
			}
		}
		if(action == 'delete'){
			statusCriteria = [statusCodes.approved, statusCodes.renterDenied, statusCodes.letterDenied, statusCodes.autoDenied];
		}
		else {
			statusCriteria = [statusCodes.awaiting, statusCodes.letterAccepted, statusCodes.renterAccepted];
		}
		matchedAds.query({where: criteria }).where('status', 'in', statusCriteria).fetch().then(function(foundEntry){
			console.log('foundEntry :: '+JSON.stringify(foundEntry));
			if(foundEntry){
				if(action == 'accept'){
					if(actionOnWhichAdType == 'letting' && foundEntry.toJSON().status == statusCodes.renterAccepted){
						status = statusCodes.approved;
					} else if(actionOnWhichAdType == 'renting' && foundEntry.toJSON().status == statusCodes.letterAccepted){
						status = statusCodes.approved;
					}
				}
				foundEntry.updateEntryStatus(status).then(function(adStatus){
					if(adStatus){
						ads.updateAdStatus(foundEntry.toJSON(), adStatus);
					}
				});
				sendSuccessJSON(req, res, next);
			}
			else {
				sendFailureJSON(req, res, next, {message: 'No such match exists'});
			}
		}, sendFailureJSON);
		
	}, sendFailureJSON);
}

var postMessages = function(req, res, next) {
	res.render('messages.html');
}

var contact = function(req, res, next) {
	res.render('content/contact.html');
}

var about = function(req, res, next) {
	res.render('content/about.html');
}

var questions = function(req, res, next) {
	res.render('content/questions.html');
}

var tips = function(req, res, next) {
	res.render('content/tips.html');
}

var howitwork = function(req, res, next) {
	res.render('content/howitwork.html');
}

var requestPassword = function(req, res, next) {
	res.render('content/requestPassword.html');
}

var passwordRequestSent = function(req, res, next) {
	res.render('content/passwordRequestSent.html');
}
var chooseregister = function(req, res, next) {
	res.render('content/chooseregister.html');
}
var confirmRegistration = function(req, res, next) {
	res.render('content/confirmRegistration.html');
}

var readConsoleErrorLogs =  function(req, res, next) {
	var logContent = fs.readFileSync(__dirname + '/../logs/console-error.log','utf8');
	res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(logContent);
    res.end();
	return;
}
var readConsoleInfoLogs =  function(req, res, next) {
	var logContent = fs.readFileSync(__dirname + '/../logs/console-info.log','utf8');
	res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(logContent);
    res.end();
	return;
}


var sendSuccessJSON = function(req, res, next, data) {
	res.json({code: '0', message: 'success', data: data});
}

var sendFailureJSON = function(req, res, next, err) {
	if(err && err.message){
		failueJSON['message'] = err.message;	
	}
	res.json(failueJSON);	
}

var sendNotAuthorizedJSON = function(req, res, next) {
	res.json(notAuthorisedJSON)
}

var authenticate = function (req, res, next) {
	if(req.isAuthenticated()) { 
		return next();
	}
	else {
		sendNotAuthorizedJSON(req, res, next);
	}
}

var checkForAuthenticationAndActive = function(req, res, next){
	if(req.isAuthenticated()) {
		new account.user({accountKey: req.session.authKey}).fetch().then(function(userAccountObj){
			if(userAccountObj && userAccountObj.get('status') =='active'){
				return next();
			}
			else {
				sendNotAuthorizedJSON(req, res, next);
			}
		});
	}
	else {
		sendNotAuthorizedJSON(req, res, next);
	}
}

//var sendMail = function(mailOptions){
//	var result='';
//	console.log('Inside Send Mail method');
////	console.log(mailOptions);
//	var transporter = nodemailer.createTransport(smtpTransport({
//	  host: mailconf.conf.host,
//	  port:mailconf.conf.port,
//	 auth: {
//	     user: mailconf.conf.username,
//	     pass: mailconf.conf.password
//	  },
//	  tls: {
//        rejectUnauthorized: false
//      }
//	}));
//	transporter.sendMail(mailOptions, function(error, info){
//		if(error){
//		    console.log(error);
//		    result=error;
//		}
//		else{
//		    console.log(info);
//		    result=info.response;      
//		}
//	});
//	console.log(result);
//}

module.exports.authenticate = authenticate;
module.exports.checkForAuthenticationAndActive = checkForAuthenticationAndActive;
//module.exports.sendMail = sendMail;
module.exports.confirmAccount =confirmMailAddress;

module.exports.getProfile = getProfile;
module.exports.getAds = getAds;
module.exports.getLettingAds = getLettingAds;
module.exports.getRentingAds = getRentingAds;
module.exports.getAdData = getAdData;
module.exports.getMessages = getMessages;

module.exports.passwordRecovery = passwordRecovery;
module.exports.changePassword = changePassword;
module.exports.putProfile = putProfile;
module.exports.postAd = postAd;
module.exports.postPayment = postPayment;
module.exports.postPaymentForAd = postPaymentForAd;
module.exports.putAd = putAd;
//module.exports.postOrPutAd = postOrPutAd;
module.exports.postMessages = postMessages;
module.exports.updateMessageStatus = updateMessageStatus;

module.exports.deleteAd = deleteAd;
module.exports.deleteAccount = deleteAccount;

module.exports.sendFailureJSON= sendFailureJSON;
module.exports.sendSuccessJSON= sendSuccessJSON;

//Unauthenticate URLS
module.exports.contact = contact;
module.exports.questions = questions;
module.exports.tips = tips;
module.exports.howitwork = howitwork;
module.exports.about = about;
module.exports.requestPassword = requestPassword;
module.exports.passwordRequestSent = passwordRequestSent;
module.exports.chooseregister = chooseregister;
module.exports.confirmRegistration = confirmRegistration;
module.exports.readConsoleErrorLogs = readConsoleErrorLogs;
module.exports.readConsoleInfoLogs = readConsoleInfoLogs;

//chooseregister



