# toh
Using nodejs.


Source structure:

	toh
		configuration -- Not yes used
		controllers -- Will have the api business logic
		model -- Will have the data object to play with database.
		routers -- Responsible for server redirections. Here we define the security rules for URLs which are to be authenticated & allowed for active users alone
		services -- Will have code related to mail sending, scheduler & matcher core functionality
		static -- Not yet used
		views -- Is the folder which contains all client related files(htmls, css, js, images & user uploaded images too.)
			  -- ** care to be taken here while updating build, the uploaded images to be backed up.
		server.js -- This is the file where app server listening port is configured.
		toh.sql -- This consists of database schema.
		run.sh & shutdown.sh -- Utility files to start/stop server


Configurations:

	Database : toh/model/db.js
	
	   connection: {
			host: '$hostname',
			user: '$user',
			password: '$password',
			database: 'toh',
			charset: 'UTF8'
		}


	Mail    : toh/controllers/mailconf.js
	
	   var conf = {
				'from':'Tak Över Huvudet <info@takoverhuvudet.se>',
				'host':'toh01.takoverhuvudet.se',
				'port':25,
				'username':'',
				'password':''
			};



	Payments : toh/views/content/login_access/adPayment.html
	
	   <input type="hidden" name="accepturl" value="http://$domain/api/ads/payment">
		Add any parameter needed here.


	Server   :
	
		web server port can be configured by here
		app.listen($port); 
		Ex: app.listen(8080);


DOs & DONTs :

	DOs: 
		1. Make sure all configuration changes are done properly
		2. Make sure toh database is created & toh.sql is populated before starting a fresh installation. There after toh.sql should never be used.
		3. Execute sh shutdown.sh & sh run.sh to stop & start server.

	DONTs: 
		1. Never change/alter/delete the uploads directory
		toh/view/uploads -- it consists of all user uploaded images.
		
Start Server:

	sh run.sh

Stop Server :

	sh shutdown.sh


