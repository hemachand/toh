
var nodemailer = require("nodemailer"),
    smtpTransport = require('nodemailer-smtp-transport');

var mailconf = require('../controllers/mailconf.js');

var sendMail = function(mailOptions){
	var result='';
	console.log('Inside Send Mail method');
//	console.log(mailOptions);
	var mailObj = {
	  host: mailconf.conf.host,
	  port:mailconf.conf.port,
	  auth: {
	     user: mailconf.conf.username,
	     pass: mailconf.conf.password
	  },
	  tls: {
        rejectUnauthorized: false
      }
	};
	if(mailconf.conf.username === ''){
		mailObj = {
		  host: mailconf.conf.host,
		  port:mailconf.conf.port
		};
	}

	console.log("Sending Mail with Mail Object::"+JSON.stringify(mailObj));

	var transporter = nodemailer.createTransport(smtpTransport(mailObj));
	transporter.sendMail(mailOptions, function(error, info){
		if(error){
		    console.log(error);
		    result=error;
		}
		else{
		    console.log(info);
		    result=info.response;      
		}
	});
	console.log(result);
}

module.exports.sendMail = sendMail;