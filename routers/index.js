var auth = require('../controllers/auth.js'),
api = require('../controllers/api.js');

module.exports = function(app){

	// Un-Authenticated URLs
	app.get('/contact', api.contact);
	app.get('/questions', api.questions);
	app.get('/about', api.about);
	app.get('/tips', api.tips);
	app.get('/howitwork', api.howitwork);
	app.get('/requestPassword', api.requestPassword);
	app.get('/passwordRequestSent', api.passwordRequestSent);
	app.get('/chooseregister', api.chooseregister);
	app.get('/confirmRegistration', api.confirmRegistration);
	app.get('/console-info', api.readConsoleInfoLogs);
	app.get('/console-error', api.readConsoleErrorLogs);

	app.get('/register/:type', auth.register);
	app.get('/register', auth.register);
	app.get('/login', auth.login);
	app.get('/home', auth.authenticate, auth.home);
	app.get('/logout', auth.authenticate, auth.logout);

//	POST
	app.post('/register', auth.registerPost);
	app.post('/login', auth.loginPost);

	// Un-Authenticated URLs
	app.get('/account/confirm/:authKey', api.confirmAccount);
	app.post('/api/account/password/recovery', api.passwordRecovery);
	app.put('/api/account/password/:authKey', api.changePassword);
	app.get('/account/password/:authKey', api.changePassword);
	app.get('/api/account/password/:authKey', api.changePassword);
	
	//	GET APIs
	app.get('/api/account/confirm/resend', api.authenticate, api.confirmAccount);
	app.get('/api/profile', api.authenticate, api.getProfile);
	app.get('/api/ads', api.checkForAuthenticationAndActive, api.getAds);
	app.get('/api/ads/letting', api.checkForAuthenticationAndActive, api.getLettingAds);
	app.get('/api/ads/renting', api.checkForAuthenticationAndActive, api.getRentingAds);
	app.get('/api/ads/letting/:adId', api.checkForAuthenticationAndActive, api.getAdData);
	app.get('/api/ads/renting/:adId', api.checkForAuthenticationAndActive, api.getAdData);
	app.get('/api/ads/:adId', api.checkForAuthenticationAndActive, api.getAdData);
	app.get('/api/messages', api.checkForAuthenticationAndActive, api.getMessages);

	//	POST APIs
	app.post('/api/ads/letting', api.checkForAuthenticationAndActive, api.postAd);
	app.post('/api/ads/renting', api.checkForAuthenticationAndActive, api.postAd);
	app.post('/api/ads/payment', api.checkForAuthenticationAndActive, api.postPayment);
//	app.post('/api/ads/payment/:adId', api.checkForAuthenticationAndActive, api.postPaymentForAd);

	//	PUT APIs
	app.put('/api/account/password', api.authenticate, api.changePassword);
	app.put('/api/profile', api.checkForAuthenticationAndActive, api.putProfile);
	app.put('/api/ads/letting/:adId', api.checkForAuthenticationAndActive, api.putAd);
	app.put('/api/ads/renting/:adId', api.checkForAuthenticationAndActive, api.putAd);
	app.put('/api/match/:action/:adType/:adId', api.checkForAuthenticationAndActive, api.updateMessageStatus);
	app.put('/api/match/:action/:adType/:adId/:matchedAdId', api.checkForAuthenticationAndActive, api.updateMessageStatus);
	

	//Detele APIs
	app.delete('/api/ads/:adId', api.checkForAuthenticationAndActive, api.deleteAd);
	app.delete('/api/account', api.authenticate, api.deleteAccount)
	app.delete('/api/account/:authKey', api.deleteAccount)
}