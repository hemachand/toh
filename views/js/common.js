var g_json,muncip_clone;
$( document ).ready(function() {
  var jDoc = $(this);
  $(document).scroll(function(){
    var top=$(this).scrollTop();
    if(top>50){
        $("header").css("background-color","rgba(94, 172, 59, 1)");
    }
    if(top<50) {
        $("header").css("background-color","rgba(94, 172, 59, 0.9)"); 
    }
  });
  
  $('#private_reg_form, #corporate_reg_form').on('submit', function(e) {
	  e.preventDefault();
	  
	  // inside event callbacks 'this' is the DOM element so we first 
	  // wrap it in a jQuery object and then invoke ajaxSubmit 
	  jQuery(this).ajaxSubmit({
		  url: '/register',
		  type: 'POST',
		  async: false,
		  success: function(data, statusText, xhr, $form){
			  console.log(data);
			  if(data.message === 'same_user'){
				  alert("Det finns redan en användare med det användarnamnet");
			  }else if(data.message === 'success'){
				  setWindowLocation('/confirmRegistration');
			  }
		  }
	  }); 

	  // !!! Important !!! 
	  // always return false to prevent standard browser submit and page navigation 
	  return false; 
  }); 
  
  $("#menuIcon").click(function () {
    $("#collapsedMenuIcon").hide();
    $("#smallScreenMenu").show();
  });
  $("#hideMenu").click(function () {
    $("#smallScreenMenu").hide();
    $("#collapsedMenuIcon").show();
  });
  $("#smallScreenMenu").hide();

  getGlobalJSON();
});

function post_login(){
	jQuery.ajax({
  	  url: '/login',
        type: 'POST',
        data: jQuery('#loginForm').serialize(),
        success: function(data) {
        	console.log(data);
        	if(data.message === 'no_username'){
        		alert("användarnamnet finns inte");
        	}else if(data.message === 'invalid_password'){
        		alert("fel lösenord");
        	}else if(data.message === 'success'){
        		setWindowLocation('/home');
        	}
      }
    });
	return false;
}

function request_password(){
  var user_mail  = jQuery('#userEmail').val();
	jQuery.ajax({
  	  url: '/api/account/password/recovery',
        type: 'POST',
        data: jQuery('#forgot_password_form').serialize(),
        success: function(data) {
        	console.log(data);
          if(data.message === 'success'){
              setWindowLocation('/passwordRequestSent');
          }else{
            alert('Användarnamnet existerar inte');
          }
        	
      }
    });
	return false;
}
function set_new_password(){
  var newPass = jQuery('#newPassword').val(), verifyPass = jQuery('#verifyNewPassword').val();
  if(newPass === verifyPass){
    jQuery.ajax({
        url: '/api'+window.location.pathname,
        type: 'PUT',
        data: jQuery('#new_password_form').serialize(),
        success: function(data) {
          console.log(data);
            if(data.message === 'success'){
                setWindowLocation('/home');
            }
        }
    });
  }else{
    alert('Fel lösenord');
  }
  return false;
}

function setWindowLocation(url){
	window.location = url;
}

function getGlobalJSON(){
	jQuery.ajax({type: "GET", async: false, dataType:"json", url: '/json/global.json', 
      success: function (data) {
      	g_json = data;
        console.log(data);
      }
  	});
}


function j_g_val(doc,id){
	return doc.find('#'+id).val();
}

$(document).ready(function() {
  jQuery(document).on('change', 'input[type="file"]', function(){
    previewImage(jQuery(this));
  });
  $(this).off('change', '#state').on('change', '#state', function(e) {
      dependencyFields();
      jQuery('#municipality').val('0');
  });
});

function previewImage(elem) {
  var fileInput = jQuery(elem)[0];
  var file = fileInput.files[0];
  if(file.size < 2097152 && (file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg')) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(file);
    oFReader.onload = function(oFREvent) {
      jQuery('label[for="'+fileInput.id+'"]').find('img').each(function(){
        jQuery(this)[0].src = oFREvent.target.result;
      });
    };
  }
  else {
    alert('Vänligen välj en bild som är mindre än 2MB');
  }
}

function dependencyFields(municp){
  var id = jQuery('#state').val();
  jQuery('#municipality').find('optgroup').remove().end().find('option').not('option[value=any],option[value=0]').remove();
  if(id === 'any' || id === '0'){
    jQuery('#municipality').val(id);
  }else{
    var opts = muncip_clone.find('optgroup#'+id).html();
    jQuery('#municipality').find('option:last').after(opts);
    if(municp != undefined){
          jQuery('#municipality').val(municp);
    }
  }
  /*jQuery.ajax({type: "GET", async: false, dataType:"json", url: '/json/state_municipality.json', 
      success: function (data) {
          console.log(data);
      }
  });*/
}
function forWhomChange() {
    var forWhom  = jQuery('#forWhom').val();
    var g_th = jQuery('#gender');
    g_th.val('any');
    if(forWhom === 'corporate'){
      g_th.hide().prev().hide();
    }else{
      g_th.show().prev().show();
    }
}