var tohControllers = angular.module('tohControllers', []);

tohControllers.controller('home_ctrl', ['$scope', '$http', function ($scope, $http) {
	$http.get('/api/profile').success(function(data) {
		if(data && data.data && data.data.userAccount){
			$scope.userAccount = data.data.userAccount;
		}else if(data.message === 'auth_failure'){
        window.location = '/home';
    }
	});
}]);

tohControllers.controller('profile_ctrl', ['$scope','$http',
  function($scope, $http) {
     $scope.g_json = g_json;
     $http.get('/api/profile').success(function(data) {
      console.log(data);
      if(data.message === 'auth_failure'){
        window.location = '/home';
      }else{
        $scope.userAccount = data.data.userAccount;
        $scope.userprofile = data.data.userAccount.profile;
        $scope.random = Math.random();
      }
    });
    $scope.edit_profile = function(user_id) {
      window.location.hash = "#/edit_profile";
    };
    $scope.delete_profile = function() {
      window.location.hash = "#/delete_profile";
    };
  }
]);
tohControllers.controller('edit_profile_ctrl', ['$scope','$http',
  function($scope, $http) {
     $http.get('/api/profile').success(function(data) {
      console.log(data);
      $scope.userAccount = data.data.userAccount;
      $scope.userprofile = data.data.userAccount.profile;
      var b_date = new Date($scope.userprofile.dob);
      $scope.s_year = b_date.getFullYear();
      $scope.s_month = b_date.getMonth()+1;
      $scope.s_date = b_date.getDate();
      $scope.update_profile = function() {
        console.log(jQuery('#edit_profile_form').serialize());
        jQuery('#edit_profile_form').ajaxSubmit({
          url: '/api/profile',
            type: 'PUT',
            success: function(data) {
              if(data.message === 'success'){
                window.location.hash = "#/myprofile";
              }
          }
        });
      };
    });
  }
]);
tohControllers.controller('delete_profile_ctrl', ['$scope','$http',
  function($scope, $http) {
     $scope.myprofile = function() {
      window.location.hash = "#/myprofile";
    };
    $scope.confirm_delete_profile = function() {
      jQuery.ajax({
          url: '/api/account',
          type: 'DELETE',
          success: function(data) {
              if(data.message === 'success'){
                  window.location = "/";
              }
          }
      });
    };
  }
]);
tohControllers.controller('change_password_ctrl', ['$scope','$http',
  function($scope, $http) {
      $scope.confirm_change_password = function() {
          var newPass = jQuery('#newPassword').val(), verifyPass = jQuery('#verifyNewPassword').val();
          if(newPass === verifyPass){
            jQuery.ajax({
                url: '/api/account/password',
                type: 'PUT',
                data:jQuery('#new_password_form').serialize(),
                success: function(data) {
                    if(data.message === 'success'){
                        window.location = "/logout";
                    }
                }
            });
          }else{
            alert('Fel lösenord');
          }
      };
  }
]);
tohControllers.controller('ads_ctrl', ['$scope','$http',
  function($scope, $http) {
    $scope.g_json = g_json;
    $http.get('/api/ads').success(function(data) {
      $scope.ads  = data.data.ads;
      $scope.userAccount = data.data.userAccount;
      console.log(data.data);
    });
    $scope.find_home = function() {
      window.location.hash = "#/find_home";
    };
    $scope.rent_home = function() {
      window.location.hash = "#/rent_home";
    };
    $scope.edit_ad = function(adType,ad_id) {
      window.location.hash = "#/edit_ad/"+adType+"/"+ad_id;
    };
    $scope.delete_ad = function(ad_id) {

      window.location.hash = "#/delete_ad/"+ad_id;
    };
    $scope.pay_ad = function(ad_id) {
      confirm_pay_ad(ad_id);
    };
  }
]);

tohControllers.controller('edit_ad_ctrl', ['$scope','$http','$routeParams',
  function($scope, $http, $routeParams) {
    loadDatePicker();
    muncip_clone = jQuery('#municipality').clone();
    var municip = '0';
    $http.get('/api/ads/'+$routeParams.ad_id).success(function(data) {
      console.log(data);
      $scope.ad_data = data.data.ad;
      municip = data.data.ad.adData.municipality;
      $scope.userAccount = data.data.userAccount;
    });

    setTimeout(function(){dependencyFields(municip);}, 500);

    $scope.edit_ad_submit = function(ad_id) {
      console.log(jQuery('#edit_ad_form').serialize());
      var url = '/api/ads/'+jQuery('#edit_ad_form').find('[name=adType]').val()+'/'+ad_id;
      jQuery('#edit_ad_form').ajaxSubmit({
        url: url,
          type: 'PUT',
          success: function(data) {
            if(data.message === 'success'){
                window.location.hash = "#/ads";
            }
        }
      });
    };
  }
]);
tohControllers.controller('delete_ad_ctrl', ['$scope','$http','$routeParams',
  function($scope, $http, $routeParams) {
    $scope.confirm_delete_ad = function() {
      var ad_id = $routeParams.ad_id;
      var url = '/api/ads/'+ad_id;
      $http.delete('/api/ads/'+ad_id).success(function(data) {
        console.log(data);
        if(data.message === 'success'){
          window.location.hash = "#/ads";
          /*$http.get('/api/ads').success(function(data) {
            $scope.ads  = data.data;
          });*/
        }
      });
    };
    $scope.go_back_ads = function() {
      window.location.hash = "#/ads";
    };
  }
]);
tohControllers.controller('messages_ctrl', ['$scope','$http','$routeParams',
  function($scope, $http,$routeParams) {
    $scope.g_json = g_json;
    $http.get('/api/messages').success(function(data) {
      console.log(data);
      $scope.let_msgs  = data.data.lettingAdObjs;
      $scope.rent_msgs  = data.data.rentingAdObjs;
      $scope.userAccount = data.data.userAccount;
    });
    $scope.accept_msg = function(ad_id,ad_type) {
      window.location.hash = "#/messages/accept/"+ad_type+"/"+ad_id;
    };
    $scope.deny_msg = function(ad_id,ad_type) {
      window.location.hash = "#/messages/deny/"+ad_type+"/"+ad_id;
    };
    $scope.confirm_accept = function() {
      var ad_id = $routeParams.ad_id, ad_type = $routeParams.ad_type;
      jQuery.ajax({
          url: '/api/match/accept/'+ad_type+'/'+ad_id,
          type: 'PUT',
          success: function(data) {
              if(data.message != 'success'){
                  alert('Kan inte acceptera erbjudandet, vänligen kontakta Tak Över Huvudet');
              }
              window.location.hash = "#/messages";
          }
      });
    };
    $scope.confirm_deny = function() {
      var ad_id = $routeParams.ad_id,  ad_type = $routeParams.ad_type;
      jQuery.ajax({
          url: '/api/match/deny/'+ad_type+'/'+ad_id,
          type: 'PUT',
          success: function(data) {
              if(data.message != 'success'){
                  alert('Kan inte neka erbjudandet, vänligen kontakta Tak Över Huvudet');
              }
              window.location.hash = "#/messages";
          }
      });
    };
    $scope.invoke_msg = function() {
      window.location.hash = "#/messages";
    };
    
  }
]);
tohControllers.controller('find_home_ctrl', ['$scope','$http',
  function($scope, $http) {
    muncip_clone = jQuery('#municipality').clone();
    $http.get('/api/profile').success(function(data) {
      if(data.message === 'auth_failure'){
        window.location = '/home';
      }else{
        $scope.userAccount = data.data.userAccount;
        $scope.userprofile = data.data.userAccount.profile;
        $scope.random = Math.random();
        loadDatePicker();
        dependencyFields();
      }
    });
  }
]);
tohControllers.controller('rent_home_ctrl', ['$scope','$http',
  function($scope, $http) {
    muncip_clone = jQuery('#municipality').clone();
    $http.get('/api/profile').success(function(data) {
      if(data.message === 'auth_failure'){
        window.location = '/home';
      }else{
        $scope.userAccount = data.data.userAccount;
        $scope.userprofile = data.data.userAccount.profile;
        $scope.random = Math.random();
        loadDatePicker();
        dependencyFields();
      }
    });
  }
]);
tohControllers.controller('find_for_rent_ctrl', ['$scope','$http',
  function($scope, $http) {
    $scope.forLettingSubmit = function() {
      jQuery.ajax({
          url: '/api/ads/letting',
          type: 'POST',
          data: jQuery('#search_accomadation').serialize(),
          success: function(data) {
              if(data.message === 'success'){
                  confirm_pay_ad(data.data.ad.id);
                  //window.location.hash = "#/ads";
              }
          }
      });
    };
  }
]);
tohControllers.controller('post_for_rent_ctrl', ['$scope','$http',
  function($scope, $http) {
  $scope.forRentsubmit = function() {
    jQuery('#rent_out_accomdation').ajaxSubmit({
      url: '/api/ads/renting',
      type: 'POST',
      success: function(data) {
        if(data.message === 'success'){
          confirm_pay_ad(data.data.ad.id);
          //window.location.hash = "#/ads";
        }
      }
    });
  };
  }
]);

function loadDatePicker(){
  $("#datepicker").datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,//this option for allowing user to select month
    changeYear: true //this option for allowing user to select from year range
  });
}
function confirm_pay_ad(ad_id){
  var dialogContainer = jQuery( "#dialog" );
  dialogContainer.load('content/login_access/adPayment.html', function(){
    dialogContainer.find("form").find('input[name="orderid"]').val(ad_id);
    dialogContainer.find("form").find('input[name="accepturl"]').val(dialogContainer.find("form").find('input[name="accepturl"]').val().replace('{{ad_id}}', ad_id));
    dialogContainer.find("form").submit();
  });
}


