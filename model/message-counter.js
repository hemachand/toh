var DB = require('./db').DB;
var knex = require('../model/db').knex;

var messageCounter = DB.Model.extend({
	tableName  : 'message_counter'
	, idAttribute: 'account_id'
		
	, incrementCount: function(accountId){
		new messageCounter({account_id: accountId}).fetch().then(function(messageCounterEntry){
			if(messageCounterEntry){
				knex('message_counter').where('account_id', '=', accountId).increment('unviewed_count', 1).then();
			}
			else {
				knex('message_counter').insert({account_id: accountId, unviewed_count: 1}).then();
			}
		});
	}
	, reinitCount: function(accountId){
		new messageCounter({account_id: accountId}).fetch().then(function(messageCounterEntry){
			if(messageCounterEntry){
				messageCounterEntry.where({account_id: accountId}).save({unviewed_count: 0}, {method:'update', patch:true}).then();
			}
		});		
	}
});

module.exports ={
	messageCounter : DB.model('messageCounter', messageCounter),
}