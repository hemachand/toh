
module.exports = {
	status : {
		matchedAds : {
			awaiting:'awaiting'
			, renterAccepted: 'renterAccepted'
			, letterAccepted: 'letterAccepted'
			, renterDenied: 'renterDenied'
			, letterDenied: 'letterDenied'
			, renterDeleted: 'renterDeleted'
			, letterDeleted: 'letterDeleted'
			, autoDenied: 'autoDenied'
			, approved:'approved'
		},
		
		ads : {
			paymentAwaited : 'paymentAwaited'
			, open:'open'
			, matched: 'matched'
			, closed: 'closed'
		}
	}
}
